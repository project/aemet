<?php

/**
 * @file
 * Hooks related to the Anonymous login module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow alter the configured city to show.
 *
 * @param string $cityToShow
 *   The actual city code.
 */
function hook_aemet_block_city_alter(&$cityToShow) {
  // Show Almonte weather.
  // The code is the province ID, 21 to Huelva,
  // and de city code, 005 to Almonte.
  $cityToShow = '21005';
}

/**
 * Allow alter the render array.
 *
 * @param string $build
 *   The actual render array.
 */
function hook_aemet_block_build_alter(&$build) {
  // Alter the block response to remove the sky icon.
  unset($build['content']['container']['sky']);
}

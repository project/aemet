# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\PrediccionesEspecificasApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**informacionNivologica_**](PrediccionesEspecificasApi.md#informacionNivologica_) | **GET** /api/prediccion/especifica/nivologica/{area} | Información nivológica.
[**prediccinDeMontaaTiempoActual_**](PrediccionesEspecificasApi.md#prediccinDeMontaaTiempoActual_) | **GET** /api/prediccion/especifica/montaña/pasada/area/{area}/dia/{dia} | Predicción de montaña. Tiempo actual.
[**prediccinDeMontaaTiempoPasado_**](PrediccionesEspecificasApi.md#prediccinDeMontaaTiempoPasado_) | **GET** /api/prediccion/especifica/montaña/pasada/area/{area} | Predicción de montaña. Tiempo pasado.
[**prediccinDeRadiacinUltravioletaUVI_**](PrediccionesEspecificasApi.md#prediccinDeRadiacinUltravioletaUVI_) | **GET** /api/prediccion/especifica/uvi/{dia} | Predicción de radiación ultravioleta (UVI).
[**prediccinParaLasPlayasTiempoActual_**](PrediccionesEspecificasApi.md#prediccinParaLasPlayasTiempoActual_) | **GET** /api/prediccion/especifica/playa/{playa} | Predicción para las playas. Tiempo actual.
[**prediccinPorMunicipiosDiariaTiempoActual_**](PrediccionesEspecificasApi.md#prediccinPorMunicipiosDiariaTiempoActual_) | **GET** /api/prediccion/especifica/municipio/diaria/{municipio} | Predicción por municipios diaria. Tiempo actual.
[**prediccinPorMunicipiosHorariaTiempoActual_**](PrediccionesEspecificasApi.md#prediccinPorMunicipiosHorariaTiempoActual_) | **GET** /api/prediccion/especifica/municipio/horaria/{municipio} | Predicción por municipios horaria. Tiempo actual.


# **informacionNivologica_**
> \Swagger\Client\Model\Model200 informacionNivologica_($area)

Información nivológica.

Información nivológica para la zona montañosa que se pasa como parámetro (area).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\PrediccionesEspecificasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$area = "area_example"; // string | | Código de  Área Montañosa |  Área Montañosa | |----------|----------| | 0 | Pirineo Catalán  | | 1  | Pirineo Navarro y Aragonés

try {
    $result = $apiInstance->informacionNivologica_($area);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrediccionesEspecificasApi->informacionNivologica_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **area** | **string**| | Código de  Área Montañosa |  Área Montañosa | |----------|----------| | 0 | Pirineo Catalán  | | 1  | Pirineo Navarro y Aragonés |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **prediccinDeMontaaTiempoActual_**
> \Swagger\Client\Model\Model200 prediccinDeMontaaTiempoActual_($area, $dia)

Predicción de montaña. Tiempo actual.

Predicción meteorológica para la zona montañosa que se pasa como parámetro (area) con validez para el día (día).  Periodicidad de actualización: continuamente.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\PrediccionesEspecificasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$area = "area_example"; // string | | Código de Área Montañosa | Área Montañosa | |----------|----------| | peu1 | Picos de Europa   | | nav1  | Pirineo Navarro   | | arn1  | Pirineo Aragonés  | | cat1  | Pirineo Catalán   | | rio1  | Ibérica Riojana   | | arn2  | Ibérica Aragonesa   | | mad2  | Sierras de Guadarrama y Somosierra  | | gre1  | Sierra de Gredos   | | nev1  | Sierra Nevada
$dia = "dia_example"; // string | | Código de día | Día | |----------|----------| | 0 | día actual  | | 1  | d+1 (mañana)   | | 2  | d+2 (pasado mañana)  | | 3  | d+3 (siguente a pasado mañana)

try {
    $result = $apiInstance->prediccinDeMontaaTiempoActual_($area, $dia);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrediccionesEspecificasApi->prediccinDeMontaaTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **area** | **string**| | Código de Área Montañosa | Área Montañosa | |----------|----------| | peu1 | Picos de Europa   | | nav1  | Pirineo Navarro   | | arn1  | Pirineo Aragonés  | | cat1  | Pirineo Catalán   | | rio1  | Ibérica Riojana   | | arn2  | Ibérica Aragonesa   | | mad2  | Sierras de Guadarrama y Somosierra  | | gre1  | Sierra de Gredos   | | nev1  | Sierra Nevada |
 **dia** | **string**| | Código de día | Día | |----------|----------| | 0 | día actual  | | 1  | d+1 (mañana)   | | 2  | d+2 (pasado mañana)  | | 3  | d+3 (siguente a pasado mañana) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **prediccinDeMontaaTiempoPasado_**
> \Swagger\Client\Model\Model200 prediccinDeMontaaTiempoPasado_($area)

Predicción de montaña. Tiempo pasado.

Breve resumen con lo más significativo de las condiciones meteorológicas registradas en la zona de montaña que se pasa como parámetro (area) en las últimas 24-36 horas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\PrediccionesEspecificasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$area = "area_example"; // string | | Código de Área Montañosa | Área Montañosa | |----------|----------| | peu1 | Picos de Europa   | | nav1  | Pirineo Navarro   | | arn1  | Pirineo Aragonés  | | cat1  | Pirineo Catalán   | | rio1  | Ibérica Riojana   | | arn2  | Ibérica Aragonesa   | | mad2  | Sierras de Guadarrama y Somosierra  | | gre1  | Sierra de Gredos   | | nev1  | Sierra Nevada

try {
    $result = $apiInstance->prediccinDeMontaaTiempoPasado_($area);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrediccionesEspecificasApi->prediccinDeMontaaTiempoPasado_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **area** | **string**| | Código de Área Montañosa | Área Montañosa | |----------|----------| | peu1 | Picos de Europa   | | nav1  | Pirineo Navarro   | | arn1  | Pirineo Aragonés  | | cat1  | Pirineo Catalán   | | rio1  | Ibérica Riojana   | | arn2  | Ibérica Aragonesa   | | mad2  | Sierras de Guadarrama y Somosierra  | | gre1  | Sierra de Gredos   | | nev1  | Sierra Nevada |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **prediccinDeRadiacinUltravioletaUVI_**
> \Swagger\Client\Model\Model200 prediccinDeRadiacinUltravioletaUVI_($dia)

Predicción de radiación ultravioleta (UVI).

Predicción de Índice de radiación UV máximo en condiciones de cielo despejado para el día seleccionado.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\PrediccionesEspecificasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dia = "dia_example"; // string | | Código de día | Día | |----------|----------| | 0 | día actual  | | 1  | d+1 (mañana)   | | 2  | d+2 (pasado mañana)  | | 3  | d+3 (dentro de 3 días) | | 4  | d+4 (dentro de 4 días)

try {
    $result = $apiInstance->prediccinDeRadiacinUltravioletaUVI_($dia);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrediccionesEspecificasApi->prediccinDeRadiacinUltravioletaUVI_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dia** | **string**| | Código de día | Día | |----------|----------| | 0 | día actual  | | 1  | d+1 (mañana)   | | 2  | d+2 (pasado mañana)  | | 3  | d+3 (dentro de 3 días) | | 4  | d+4 (dentro de 4 días) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **prediccinParaLasPlayasTiempoActual_**
> \Swagger\Client\Model\Model200 prediccinParaLasPlayasTiempoActual_($playa)

Predicción para las playas. Tiempo actual.

La predicción diaria de la playa que se pasa como parámetro. Establece el estado de nubosidad para unas horas determinadas, las 11 y las 17 hora oficial. Se analiza también si se espera precipitación en el entorno de esas horas, entre las 08 y las 14 horas y entre las 14 y 20 horas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\PrediccionesEspecificasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$playa = "playa_example"; // string | Código de playa   http://www.aemet.es/documentos/es/eltiempo/prediccion/playas/Playas_codigos.csv

try {
    $result = $apiInstance->prediccinParaLasPlayasTiempoActual_($playa);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrediccionesEspecificasApi->prediccinParaLasPlayasTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **playa** | **string**| Código de playa   http://www.aemet.es/documentos/es/eltiempo/prediccion/playas/Playas_codigos.csv |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **prediccinPorMunicipiosDiariaTiempoActual_**
> \Swagger\Client\Model\Model200 prediccinPorMunicipiosDiariaTiempoActual_($municipio)

Predicción por municipios diaria. Tiempo actual.

Predicción para el municipio que se pasa como parámetro (municipio). Periodicidad de actualización: continuamente.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\PrediccionesEspecificasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$municipio = "municipio_example"; // string | Código de municipio   http://www.ine.es/daco/daco42/codmun/codmunmapa.htm

try {
    $result = $apiInstance->prediccinPorMunicipiosDiariaTiempoActual_($municipio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrediccionesEspecificasApi->prediccinPorMunicipiosDiariaTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **municipio** | **string**| Código de municipio   http://www.ine.es/daco/daco42/codmun/codmunmapa.htm |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **prediccinPorMunicipiosHorariaTiempoActual_**
> \Swagger\Client\Model\Model200 prediccinPorMunicipiosHorariaTiempoActual_($municipio)

Predicción por municipios horaria. Tiempo actual.

Predicción horaria para el municipio que se pasa como parámetro (municipio). Presenta la información de hora en hora hasta 48 horas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\PrediccionesEspecificasApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$municipio = "municipio_example"; // string | Código de municipio  http://www.ine.es/daco/daco42/codmun/codmunmapa.htm

try {
    $result = $apiInstance->prediccinPorMunicipiosHorariaTiempoActual_($municipio);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrediccionesEspecificasApi->prediccinPorMunicipiosHorariaTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **municipio** | **string**| Código de municipio  http://www.ine.es/daco/daco42/codmun/codmunmapa.htm |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\ValoresClimatologicosApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**climatologasDiarias1**](ValoresClimatologicosApi.md#climatologasDiarias1) | **GET** /api/valores/climatologicos/diarios/datos/fechaini/{fechaIniStr}/fechafin/{fechaFinStr}/todasestaciones | Climatologías diarias.
[**climatologasDiarias_**](ValoresClimatologicosApi.md#climatologasDiarias_) | **GET** /api/valores/climatologicos/diarios/datos/fechaini/{fechaIniStr}/fechafin/{fechaFinStr}/estacion/{idema} | Climatologías diarias.
[**climatologasMensualesAnuales_**](ValoresClimatologicosApi.md#climatologasMensualesAnuales_) | **GET** /api/valores/climatologicos/mensualesanuales/datos/anioini/{anioIniStr}/aniofin/{anioFinStr}/estacion/{idema} | Climatologías mensuales anuales.
[**climatologasNormales19812010_**](ValoresClimatologicosApi.md#climatologasNormales19812010_) | **GET** /api/valores/climatologicos/normales/estacion/{idema} | Climatologías normales (1981-2010).
[**estacionesPorIndicativo_**](ValoresClimatologicosApi.md#estacionesPorIndicativo_) | **GET** /api/valores/climatologicos/inventarioestaciones/estaciones/{estaciones} | Estaciones por indicativo.
[**inventarioDeEstacionesValoresClimatolgicos_**](ValoresClimatologicosApi.md#inventarioDeEstacionesValoresClimatolgicos_) | **GET** /api/valores/climatologicos/inventarioestaciones/todasestaciones | Inventario de estaciones (valores climatológicos).
[**valoresExtremos_**](ValoresClimatologicosApi.md#valoresExtremos_) | **GET** /api/valores/climatologicos/valoresextremos/parametro/{parametro}/estacion/{idema} | Valores extremos.


# **climatologasDiarias1**
> \Swagger\Client\Model\Model200 climatologasDiarias1($fecha_ini_str, $fecha_fin_str)

Climatologías diarias.

Valores climatológicos de todas las estaciones para el rango de fechas seleccionado. Periodicidad: 1 vez al día.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ValoresClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$fecha_ini_str = "fecha_ini_str_example"; // string | Fecha Inicial (AAAA-MM-DDTHH:MM:SSUTC)
$fecha_fin_str = "fecha_fin_str_example"; // string | Fecha Final (AAAA-MM-DDTHH:MM:SSUTC)

try {
    $result = $apiInstance->climatologasDiarias1($fecha_ini_str, $fecha_fin_str);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ValoresClimatologicosApi->climatologasDiarias1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fecha_ini_str** | **string**| Fecha Inicial (AAAA-MM-DDTHH:MM:SSUTC) |
 **fecha_fin_str** | **string**| Fecha Final (AAAA-MM-DDTHH:MM:SSUTC) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **climatologasDiarias_**
> \Swagger\Client\Model\Model200 climatologasDiarias_($fecha_ini_str, $fecha_fin_str, $idema)

Climatologías diarias.

Valores climatológicos para el rango de fechas y la estación seleccionada. Periodicidad: 1 vez al día.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ValoresClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$fecha_ini_str = "fecha_ini_str_example"; // string | Fecha Inicial (AAAA-MM-DDTHH:MM:SSUTC)
$fecha_fin_str = "fecha_fin_str_example"; // string | Fecha Final (AAAA-MM-DDTHH:MM:SSUTC)
$idema = "idema_example"; // string | Indicativo climatológico de la EMA. Puede introducir varios indicativos separados por comas (,)

try {
    $result = $apiInstance->climatologasDiarias_($fecha_ini_str, $fecha_fin_str, $idema);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ValoresClimatologicosApi->climatologasDiarias_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fecha_ini_str** | **string**| Fecha Inicial (AAAA-MM-DDTHH:MM:SSUTC) |
 **fecha_fin_str** | **string**| Fecha Final (AAAA-MM-DDTHH:MM:SSUTC) |
 **idema** | **string**| Indicativo climatológico de la EMA. Puede introducir varios indicativos separados por comas (,) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **climatologasMensualesAnuales_**
> \Swagger\Client\Model\Model200 climatologasMensualesAnuales_($anio_ini_str, $anio_fin_str, $idema)

Climatologías mensuales anuales.

Valores medios mensuales y anuales de los datos climatológicos para la estación y el periodo de años pasados por parámetro. Periodicidad: 1 vez al día.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ValoresClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$anio_ini_str = "anio_ini_str_example"; // string | Año Inicial (AAAA)
$anio_fin_str = "anio_fin_str_example"; // string | Año Final (AAAA)
$idema = "idema_example"; // string | Indicativo climatológico de la EMA

try {
    $result = $apiInstance->climatologasMensualesAnuales_($anio_ini_str, $anio_fin_str, $idema);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ValoresClimatologicosApi->climatologasMensualesAnuales_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anio_ini_str** | **string**| Año Inicial (AAAA) |
 **anio_fin_str** | **string**| Año Final (AAAA) |
 **idema** | **string**| Indicativo climatológico de la EMA |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **climatologasNormales19812010_**
> \Swagger\Client\Model\Model200 climatologasNormales19812010_($idema)

Climatologías normales (1981-2010).

Valores climatológicos normales (periodo 1981-2010) para la estación pasada por parámetro. Periodicidad: 1 vez al día.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ValoresClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$idema = "idema_example"; // string | Indicativo climatológico de la EMA

try {
    $result = $apiInstance->climatologasNormales19812010_($idema);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ValoresClimatologicosApi->climatologasNormales19812010_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idema** | **string**| Indicativo climatológico de la EMA |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **estacionesPorIndicativo_**
> \Swagger\Client\Model\Model200 estacionesPorIndicativo_($estaciones)

Estaciones por indicativo.

Características de la estación climatológica pasada por parámetro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ValoresClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$estaciones = "estaciones_example"; // string | Listado de indicativos climatológicos (id1,id2,id3,...,idn)

try {
    $result = $apiInstance->estacionesPorIndicativo_($estaciones);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ValoresClimatologicosApi->estacionesPorIndicativo_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **estaciones** | **string**| Listado de indicativos climatológicos (id1,id2,id3,...,idn) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **inventarioDeEstacionesValoresClimatolgicos_**
> \Swagger\Client\Model\Model200 inventarioDeEstacionesValoresClimatolgicos_()

Inventario de estaciones (valores climatológicos).

Inventario con las características de todas las estaciones climatológicas. Periodicidad: 1 vez al día.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ValoresClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->inventarioDeEstacionesValoresClimatolgicos_();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ValoresClimatologicosApi->inventarioDeEstacionesValoresClimatolgicos_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **valoresExtremos_**
> \Swagger\Client\Model\Model200 valoresExtremos_($parametro, $idema)

Valores extremos.

Valores extremos para la estación y la variable (precipitación, temperatura y viento) pasadas por parámetro. Periodicidad: 1 vez al día.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ValoresClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$parametro = "parametro_example"; // string | | Código | Parámetro Meteorológico | |----------|----------| | P  | Precipitación   | | T  | Temperatura   | | V  | Viento
$idema = "idema_example"; // string | Indicativo climatológico de la EMA

try {
    $result = $apiInstance->valoresExtremos_($parametro, $idema);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ValoresClimatologicosApi->valoresExtremos_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **parametro** | **string**| | Código | Parámetro Meteorológico | |----------|----------| | P  | Precipitación   | | T  | Temperatura   | | V  | Viento |
 **idema** | **string**| Indicativo climatológico de la EMA |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


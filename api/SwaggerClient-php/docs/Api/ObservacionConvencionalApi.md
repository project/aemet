# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\ObservacionConvencionalApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**datosDeObservacinTiempoActual1**](ObservacionConvencionalApi.md#datosDeObservacinTiempoActual1) | **GET** /api/observacion/convencional/datos/estacion/{idema} | Datos de observación. Tiempo actual.
[**datosDeObservacinTiempoActual_**](ObservacionConvencionalApi.md#datosDeObservacinTiempoActual_) | **GET** /api/observacion/convencional/todas | Datos de observación. Tiempo actual.
[**mensajesDeObservacinLtimoElaborado_**](ObservacionConvencionalApi.md#mensajesDeObservacinLtimoElaborado_) | **GET** /api/observacion/convencional/mensajes/tipomensaje/{tipomensaje} | Mensajes de observación. Último elaborado.


# **datosDeObservacinTiempoActual1**
> \Swagger\Client\Model\Model200 datosDeObservacinTiempoActual1($idema)

Datos de observación. Tiempo actual.

Datos de observación horarios de las últimas 24 horas de la estación meterológica que se pasa como parámetro (idema). Frecuencia de actualización: continuamente.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ObservacionConvencionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$idema = "idema_example"; // string | Índicativo climatológico de la EMA

try {
    $result = $apiInstance->datosDeObservacinTiempoActual1($idema);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ObservacionConvencionalApi->datosDeObservacinTiempoActual1: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idema** | **string**| Índicativo climatológico de la EMA |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **datosDeObservacinTiempoActual_**
> \Swagger\Client\Model\Model200 datosDeObservacinTiempoActual_()

Datos de observación. Tiempo actual.

Datos de observación horarios de las últimas 24 horas todas las estaciones meteorológicas de las que se han recibido datos en ese período. Frecuencia de actualización: continuamente.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ObservacionConvencionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->datosDeObservacinTiempoActual_();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ObservacionConvencionalApi->datosDeObservacinTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mensajesDeObservacinLtimoElaborado_**
> \Swagger\Client\Model\Model200 mensajesDeObservacinLtimoElaborado_($tipomensaje)

Mensajes de observación. Último elaborado.

Últimos mensajes de observación. Para los SYNOP y TEMP devuelve los mensajes de las últimas 24 horas y para los CLIMAT de los 40 últimos dias. Se pasa como parámetro el tipo de mensaje que se desea (tipomensaje). El resultado de la petición es un fichero en formato tar.gz, que contiene los boletines en formato json y bufr.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ObservacionConvencionalApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tipomensaje = "tipomensaje_example"; // string | | Código | Tipo de Mensaje | |----------|----------| | climat  | CLIMAT   | | synop  | SYNOP   | | temp  | TEMP

try {
    $result = $apiInstance->mensajesDeObservacinLtimoElaborado_($tipomensaje);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ObservacionConvencionalApi->mensajesDeObservacinLtimoElaborado_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipomensaje** | **string**| | Código | Tipo de Mensaje | |----------|----------| | climat  | CLIMAT   | | synop  | SYNOP   | | temp  | TEMP |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\IndicesIncendiosApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**mapaDeNivelesDeRiesgoEstimadoMeteorolgicoDeIncendiosForestales_**](IndicesIncendiosApi.md#mapaDeNivelesDeRiesgoEstimadoMeteorolgicoDeIncendiosForestales_) | **GET** /api/incendios/mapasriesgo/estimado/area/{area} | Mapa de niveles de riesgo estimado meteorológico de incendios forestales.
[**mapaDeNivelesDeRiesgoPrevistoMeteorolgicoDeIncendiosForestales_**](IndicesIncendiosApi.md#mapaDeNivelesDeRiesgoPrevistoMeteorolgicoDeIncendiosForestales_) | **GET** /api/incendios/mapasriesgo/previsto/dia/{dia}/area/{area} | Mapa de niveles de riesgo previsto meteorológico de incendios forestales.


# **mapaDeNivelesDeRiesgoEstimadoMeteorolgicoDeIncendiosForestales_**
> \Swagger\Client\Model\Model200 mapaDeNivelesDeRiesgoEstimadoMeteorolgicoDeIncendiosForestales_($area)

Mapa de niveles de riesgo estimado meteorológico de incendios forestales.

Último mapa elaborado de niveles de riesgo estimado meteorológico de incendios forestales para el área pasada por parámetro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\IndicesIncendiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$area = "area_example"; // string | | Código | Área | |----------|----------| | p  | Península   | | b  | Baleares   | | c  | Canarias

try {
    $result = $apiInstance->mapaDeNivelesDeRiesgoEstimadoMeteorolgicoDeIncendiosForestales_($area);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesIncendiosApi->mapaDeNivelesDeRiesgoEstimadoMeteorolgicoDeIncendiosForestales_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **area** | **string**| | Código | Área | |----------|----------| | p  | Península   | | b  | Baleares   | | c  | Canarias |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **mapaDeNivelesDeRiesgoPrevistoMeteorolgicoDeIncendiosForestales_**
> \Swagger\Client\Model\Model200 mapaDeNivelesDeRiesgoPrevistoMeteorolgicoDeIncendiosForestales_($dia, $area)

Mapa de niveles de riesgo previsto meteorológico de incendios forestales.

Mapa elaborado de niveles de riesgo estimado meteorológico de incendios forestales para el día y el área pasados por parámetro.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\IndicesIncendiosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$dia = "dia_example"; // string | | Código | Día | |----------|----------| | 1  | Mañana   | | 2  | Pasado Mañana   | | 3  | Dentro de 3 días
$area = "area_example"; // string | | Código | Área | |----------|----------| | p  | Península   | | b  | Baleares   | | c  | Canarias

try {
    $result = $apiInstance->mapaDeNivelesDeRiesgoPrevistoMeteorolgicoDeIncendiosForestales_($dia, $area);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling IndicesIncendiosApi->mapaDeNivelesDeRiesgoPrevistoMeteorolgicoDeIncendiosForestales_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dia** | **string**| | Código | Día | |----------|----------| | 1  | Mañana   | | 2  | Pasado Mañana   | | 3  | Dentro de 3 días |
 **area** | **string**| | Código | Área | |----------|----------| | p  | Península   | | b  | Baleares   | | c  | Canarias |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\RedRayosApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**mapaConLosRayosRegistradosEnPeriodoStandardLtimoElaborado_**](RedRayosApi.md#mapaConLosRayosRegistradosEnPeriodoStandardLtimoElaborado_) | **GET** /api/red/rayos/mapa | Mapa con los rayos registrados en periodo standard. Último elaborado.


# **mapaConLosRayosRegistradosEnPeriodoStandardLtimoElaborado_**
> \Swagger\Client\Model\Model200 mapaConLosRayosRegistradosEnPeriodoStandardLtimoElaborado_()

Mapa con los rayos registrados en periodo standard. Último elaborado.

Imagen de las descargas caídas en el territorio nacional durante un período de 12 horas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\RedRayosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->mapaConLosRayosRegistradosEnPeriodoStandardLtimoElaborado_();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RedRayosApi->mapaConLosRayosRegistradosEnPeriodoStandardLtimoElaborado_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


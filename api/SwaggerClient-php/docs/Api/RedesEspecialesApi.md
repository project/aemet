# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\RedesEspecialesApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**contenidoTotalDeOzonoTiempoActual_**](RedesEspecialesApi.md#contenidoTotalDeOzonoTiempoActual_) | **GET** /api/red/especial/ozono | Contenido total de ozono. Tiempo actual.
[**datosDeContaminacinDeFondoTiempoActual_**](RedesEspecialesApi.md#datosDeContaminacinDeFondoTiempoActual_) | **GET** /api/red/especial/contaminacionfondo/estacion/{nombre_estacion} | Datos de contaminación de fondo. Tiempo actual.
[**datosDeRadiacinGlobalDirectaODifusaTiempoActual_**](RedesEspecialesApi.md#datosDeRadiacinGlobalDirectaODifusaTiempoActual_) | **GET** /api/red/especial/radiacion | Datos de radiación global, directa o difusa. Tiempo actual.
[**perfilesVerticalesDeOzonoTiempoActual_**](RedesEspecialesApi.md#perfilesVerticalesDeOzonoTiempoActual_) | **GET** /api/red/especial/perfilozono/estacion/{estacion} | Perfiles verticales de ozono. Tiempo actual.


# **contenidoTotalDeOzonoTiempoActual_**
> \Swagger\Client\Model\Model200 contenidoTotalDeOzonoTiempoActual_()

Contenido total de ozono. Tiempo actual.

Dato medio diario de contenido total de ozono. Cada 24 h (actualmente, en fines de semana, festivos y vacaciones no se genera por la falta de personal en el Centro Radiométrico Nacional).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\RedesEspecialesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->contenidoTotalDeOzonoTiempoActual_();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RedesEspecialesApi->contenidoTotalDeOzonoTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **datosDeContaminacinDeFondoTiempoActual_**
> \Swagger\Client\Model\Model200 datosDeContaminacinDeFondoTiempoActual_($nombre_estacion)

Datos de contaminación de fondo. Tiempo actual.

Ficheros diarios con datos diezminutales de la estación de la red de contaminación de fondo EMEP/VAG/CAMP pasada por parámetro, de temperatura, presión, humedad, viento (dirección y velocidad), radiación global, precipitación y 4 componentes químicos: O3,SO2,NO,NO2 y PM10. Los datos se encuentran en formato FINN (propio del Ministerio de Medio Ambiente). Periodicidad: cada hora.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\RedesEspecialesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$nombre_estacion = "nombre_estacion_example"; // string | | Código | Estación de la Red EMEP | |----------|----------| | 11  | Barcarrota (Badajoz)   | | 10  | Cabo de Creus (Girona)   | | 09  | Campisábalos (Guadalajara)   | | 17  | Doñana (Huelva)  | | 14  | Els Torms (Lleida)   | | 06  | Mahón (Illes Balears)   | | 08  | Niembro-Llanes (Asturias)   | | 05  | Noia (A Coruña)   | | 16  | O Saviñao (Lugo)   | | 13  | Peñausende (Zamora)   | | 01  | San Pablo de los Montes (Toledo)   | | 07  | Víznar (Granada)   | | 12  | Zarra (Valencia)

try {
    $result = $apiInstance->datosDeContaminacinDeFondoTiempoActual_($nombre_estacion);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RedesEspecialesApi->datosDeContaminacinDeFondoTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nombre_estacion** | **string**| | Código | Estación de la Red EMEP | |----------|----------| | 11  | Barcarrota (Badajoz)   | | 10  | Cabo de Creus (Girona)   | | 09  | Campisábalos (Guadalajara)   | | 17  | Doñana (Huelva)  | | 14  | Els Torms (Lleida)   | | 06  | Mahón (Illes Balears)   | | 08  | Niembro-Llanes (Asturias)   | | 05  | Noia (A Coruña)   | | 16  | O Saviñao (Lugo)   | | 13  | Peñausende (Zamora)   | | 01  | San Pablo de los Montes (Toledo)   | | 07  | Víznar (Granada)   | | 12  | Zarra (Valencia) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **datosDeRadiacinGlobalDirectaODifusaTiempoActual_**
> \Swagger\Client\Model\Model200 datosDeRadiacinGlobalDirectaODifusaTiempoActual_()

Datos de radiación global, directa o difusa. Tiempo actual.

Datos horarios (HORA SOLAR VERDADERA) acumulados de radiación  global, directa, difusa e infrarroja, y datos semihorarios  (HORA SOLAR VERDADERA) acumulados de radiación ultravioleta eritemática.Datos diarios acumulados  de radiación global, directa, difusa, ultravioleta eritemática e infrarroja. Periodicidad: Cada 24h (actualmente en fines de semana, festivos y vacaciones, no se genera por la ausencia de personal en el Centro Radiométrico Nacional).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\RedesEspecialesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->datosDeRadiacinGlobalDirectaODifusaTiempoActual_();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RedesEspecialesApi->datosDeRadiacinGlobalDirectaODifusaTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **perfilesVerticalesDeOzonoTiempoActual_**
> \Swagger\Client\Model\Model200 perfilesVerticalesDeOzonoTiempoActual_($estacion)

Perfiles verticales de ozono. Tiempo actual.

Perfil Vertical de Ozono de la estación pasada por parámetro. Periodicidad: cada 7 días.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\RedesEspecialesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$estacion = "estacion_example"; // string | | Código | Estación | |----------|----------| | canarias  | Izaña   | | peninsula  | Madrid

try {
    $result = $apiInstance->perfilesVerticalesDeOzonoTiempoActual_($estacion);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RedesEspecialesApi->perfilesVerticalesDeOzonoTiempoActual_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **estacion** | **string**| | Código | Estación | |----------|----------| | canarias  | Izaña   | | peninsula  | Madrid |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


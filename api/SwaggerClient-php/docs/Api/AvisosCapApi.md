# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\AvisosCapApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**avisosDeFenmenosMeteorolgicosAdversosArchivo**](AvisosCapApi.md#avisosDeFenmenosMeteorolgicosAdversosArchivo) | **GET** /api/avisos_cap/archivo/fechaini/{fechaIniStr}/fechafin/{fechaFinStr} | Avisos de Fenómenos Meteorológicos Adversos. Archivo.
[**avisosDeFenmenosMeteorolgicosAdversosLtimo_**](AvisosCapApi.md#avisosDeFenmenosMeteorolgicosAdversosLtimo_) | **GET** /api/avisos_cap/ultimoelaborado/area/{area} | Avisos de Fenómenos Meteorológicos Adversos. Último.


# **avisosDeFenmenosMeteorolgicosAdversosArchivo**
> \Swagger\Client\Model\Model200 avisosDeFenmenosMeteorolgicosAdversosArchivo($fecha_ini_str, $fecha_fin_str)

Avisos de Fenómenos Meteorológicos Adversos. Archivo.

Avisos de Fenómenos Meteorológicos adversos para el rango de fechas seleccionado (datos desde 18/06/2018).

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\AvisosCapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$fecha_ini_str = "fecha_ini_str_example"; // string | Fecha Inicial (AAAA-MM-DDTHH:MM:SSUTC)
$fecha_fin_str = "fecha_fin_str_example"; // string | Fecha Final (AAAA-MM-DDTHH:MM:SSUTC)

try {
    $result = $apiInstance->avisosDeFenmenosMeteorolgicosAdversosArchivo($fecha_ini_str, $fecha_fin_str);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AvisosCapApi->avisosDeFenmenosMeteorolgicosAdversosArchivo: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fecha_ini_str** | **string**| Fecha Inicial (AAAA-MM-DDTHH:MM:SSUTC) |
 **fecha_fin_str** | **string**| Fecha Final (AAAA-MM-DDTHH:MM:SSUTC) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **avisosDeFenmenosMeteorolgicosAdversosLtimo_**
> \Swagger\Client\Model\Model200 avisosDeFenmenosMeteorolgicosAdversosLtimo_($area)

Avisos de Fenómenos Meteorológicos Adversos. Último.

Últimos Avisos de Fenómenos Meteorológicos adversos elaborado para el área seleccionada.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\AvisosCapApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$area = "area_example"; // string | | Código | Área | |----------|----------| | esp  | España| | 61  | Andalucía   | | 62  | Aragón   | | 63  | Asturias, Principado de  | | 64  | Ballears, Illes   | | 78  | Ceuta   | | 65  | Canarias   | | 66  | Cantabria   | | 67  | Castilla y León   | | 68  | Castilla - La Mancha   | | 69  | Cataluña   | | 77  | Comunitat Valenciana   | | 70  | Extremadura   | | 71  | Galicia   | | 72  | Madrid, Comunidad de    | | 79  | Melilla   | | 73  | Murcia, Región de   | | 74  | Navarra, Comunidad Foral de   | | 75  | País Vasco | | 76  | Rioja, La

try {
    $result = $apiInstance->avisosDeFenmenosMeteorolgicosAdversosLtimo_($area);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AvisosCapApi->avisosDeFenmenosMeteorolgicosAdversosLtimo_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **area** | **string**| | Código | Área | |----------|----------| | esp  | España| | 61  | Andalucía   | | 62  | Aragón   | | 63  | Asturias, Principado de  | | 64  | Ballears, Illes   | | 78  | Ceuta   | | 65  | Canarias   | | 66  | Cantabria   | | 67  | Castilla y León   | | 68  | Castilla - La Mancha   | | 69  | Cataluña   | | 77  | Comunitat Valenciana   | | 70  | Extremadura   | | 71  | Galicia   | | 72  | Madrid, Comunidad de    | | 79  | Melilla   | | 73  | Murcia, Región de   | | 74  | Navarra, Comunidad Foral de   | | 75  | País Vasco | | 76  | Rioja, La |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


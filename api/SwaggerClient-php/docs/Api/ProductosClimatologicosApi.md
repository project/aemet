# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Swagger\Client\ProductosClimatologicosApi

All URIs are relative to *https://opendata.aemet.es/opendata*

Method | HTTP request | Description
------------- | ------------- | -------------
[**balanceHdricoNacionalDocumento_**](ProductosClimatologicosApi.md#balanceHdricoNacionalDocumento_) | **GET** /api/productos/climatologicos/balancehidrico/{anio}/{decena} | Balance hídrico nacional (documento).
[**capasSHAPEDeEstacionesClimatolgicas_**](ProductosClimatologicosApi.md#capasSHAPEDeEstacionesClimatolgicas_) | **GET** /api/productos/climatologicos/capasshape/{tipoestacion} | Capas SHAPE de estaciones climatológicas de AEMET.
[**resumenMensualClimatolgicoNacionalDocumento_**](ProductosClimatologicosApi.md#resumenMensualClimatolgicoNacionalDocumento_) | **GET** /api/productos/climatologicos/resumenclimatologico/nacional/{anio}/{mes} | Resumen mensual climatológico nacional (documento).


# **balanceHdricoNacionalDocumento_**
> \Swagger\Client\Model\Model200 balanceHdricoNacionalDocumento_($anio, $decena)

Balance hídrico nacional (documento).

Se obtiene, para la decema y el año pasados por parámetro, el Boletín Hídrico Nacional que se elabora cada diez días. Se presenta información resumida de forma distribuida para todo el territorio nacional de diferentes variables, en las que se incluye informaciones de la precipitación y la evapotranspiración potencial acumuladas desde el 1 de septiembre.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductosClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$anio = "anio_example"; // string | Año (AAAA)
$decena = "decena_example"; // string | Decena de 01 (primera decena) a 36 (última decena)

try {
    $result = $apiInstance->balanceHdricoNacionalDocumento_($anio, $decena);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductosClimatologicosApi->balanceHdricoNacionalDocumento_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anio** | **string**| Año (AAAA) |
 **decena** | **string**| Decena de 01 (primera decena) a 36 (última decena) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **capasSHAPEDeEstacionesClimatolgicas_**
> \Swagger\Client\Model\Model200 capasSHAPEDeEstacionesClimatolgicas_($tipoestacion)

Capas SHAPE de estaciones climatológicas de AEMET.

Capas SHAPE de las distintas estaciones climatológicas de AEMET: automáticas, completas, pluviométricas y termométricas.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductosClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tipoestacion = "tipoestacion_example"; // string | | Código | Tipo de Estación | |----------|----------| | automaticas  | Estaciones Automáticas   | | completas  | Estaciones Completas   | | pluviometricas  | Estaciones Pluviométricas   | | termometricas  | Estaciones Termométricas

try {
    $result = $apiInstance->capasSHAPEDeEstacionesClimatolgicas_($tipoestacion);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductosClimatologicosApi->capasSHAPEDeEstacionesClimatolgicas_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tipoestacion** | **string**| | Código | Tipo de Estación | |----------|----------| | automaticas  | Estaciones Automáticas   | | completas  | Estaciones Completas   | | pluviometricas  | Estaciones Pluviométricas   | | termometricas  | Estaciones Termométricas |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **resumenMensualClimatolgicoNacionalDocumento_**
> \Swagger\Client\Model\Model200 resumenMensualClimatolgicoNacionalDocumento_($anio, $mes)

Resumen mensual climatológico nacional (documento).

Resumen climatológico nacional, para el año y mes pasado por parámetro, sobre el estado del clima y la evolución de las principales variables climáticas, en especial temperatura y precipitación, a nivel mensual, estacional y anual.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: api_key
$config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKey('api_key', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = Swagger\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('api_key', 'Bearer');

$apiInstance = new Swagger\Client\Api\ProductosClimatologicosApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$anio = "anio_example"; // string | Año (AAAA)
$mes = "mes_example"; // string | Mes (mm)

try {
    $result = $apiInstance->resumenMensualClimatolgicoNacionalDocumento_($anio, $mes);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductosClimatologicosApi->resumenMensualClimatolgicoNacionalDocumento_: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anio** | **string**| Año (AAAA) |
 **mes** | **string**| Mes (mm) |

### Return type

[**\Swagger\Client\Model\Model200**](../Model/Model200.md)

### Authorization

[api_key](../../README.md#api_key)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)


# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Model200

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descripcion** | **string** |  | [default to 'Éxito']
**estado** | **int** |  | 
**datos** | **string** |  | 
**metadatos** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



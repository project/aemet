# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Model429

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descripcion** | **string** |  | [default to 'Too Many Requests']
**estado** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



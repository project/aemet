# @codingStandardsIgnoreFile
# phpcs:ignoreFile

# Model404

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**descripcion** | **string** |  | [default to 'Not Found']
**estado** | **int** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



<?php
// @codingStandardsIgnoreFile
// phpcs:ignoreFile

/**
 * PrediccionesEspecificasApiTest
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * AEMET OpenData
 *
 * AEMET OpenData es una API REST desarrollado por AEMET que permite la difusión y la reutilización de la información meteorológica y climatológica de la Agencia, en el sentido indicado en la Ley 18/2015, de 9 de julio, por la que se modifica la Ley 37/2007, de 16 de noviembre, sobre reutilización de la información del sector público. (IMPORTANTE: Para poder realizar peticiones, es necesario introducir en API Key haciendo clic en el círculo rojo de recurso REST).
 *
 * OpenAPI spec version: 2.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.4.0
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace Swagger\Client;

use \Swagger\Client\Configuration;
use \Swagger\Client\ApiException;
use \Swagger\Client\ObjectSerializer;

/**
 * PrediccionesEspecificasApiTest Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class PrediccionesEspecificasApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for informacionNivologica_
     *
     * Información nivológica..
     *
     */
    public function testInformacionNivologica_()
    {
    }

    /**
     * Test case for prediccinDeMontaaTiempoActual_
     *
     * Predicción de montaña. Tiempo actual..
     *
     */
    public function testPrediccinDeMontaaTiempoActual_()
    {
    }

    /**
     * Test case for prediccinDeMontaaTiempoPasado_
     *
     * Predicción de montaña. Tiempo pasado..
     *
     */
    public function testPrediccinDeMontaaTiempoPasado_()
    {
    }

    /**
     * Test case for prediccinDeRadiacinUltravioletaUVI_
     *
     * Predicción de radiación ultravioleta (UVI)..
     *
     */
    public function testPrediccinDeRadiacinUltravioletaUVI_()
    {
    }

    /**
     * Test case for prediccinParaLasPlayasTiempoActual_
     *
     * Predicción para las playas. Tiempo actual..
     *
     */
    public function testPrediccinParaLasPlayasTiempoActual_()
    {
    }

    /**
     * Test case for prediccinPorMunicipiosDiariaTiempoActual_
     *
     * Predicción por municipios diaria. Tiempo actual..
     *
     */
    public function testPrediccinPorMunicipiosDiariaTiempoActual_()
    {
    }

    /**
     * Test case for prediccinPorMunicipiosHorariaTiempoActual_
     *
     * Predicción por municipios horaria. Tiempo actual..
     *
     */
    public function testPrediccinPorMunicipiosHorariaTiempoActual_()
    {
    }
}

<?php

namespace Drupal\aemet\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\aemet\Values as AemetValues;
use Swagger\Client\Api\PrediccionesEspecificasApi;
use Swagger\Client\Configuration;
use Drupal\Core\Config\ConfigFactory;
use Psr\Log\LoggerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Url;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides a simple aemet weather block.
 *
 * @Block(
 *   id = "aemet_simple_aemet_weather",
 *   admin_label = @Translation("Simple AEMET weather"),
 *   category = @Translation("Weather")
 * )
 */
class SimpleAemetWeatherBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Aemet values.
   *
   * @var \Drupal\aemet\Values
   */
  protected $aemetValues;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('aemet.values'),
      $container->get('config.factory'),
      $container->get('logger.factory')->get('Simple_Aemet_Weather_Block'),
      $container->get('module_handler')
    );
  }

  /**
   * Creates a SimpleAemetWeatherBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\aemet\Values $aemetValues
   *   Aemet values.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Config factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AemetValues $aemetValues,
    ConfigFactory $configFactory,
    LoggerInterface $logger,
    ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->aemetValues = $aemetValues;
    $this->configFactory = $configFactory;
    $this->logger = $logger;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    $max_age = parent::getCacheMaxAge();

    if ($max_age == Cache::PERMANENT) {
      // Six hours.
      $max_age = 6 * 60 * 60;
    }

    return $max_age;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'city' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['city'] = [
      '#type' => 'select',
      '#title' => $this->t('City'),
      '#options' => $this->flatCities(),
      '#default_value' => $this->configuration['city'],
    ];
    return $form;
  }

  /**
   * Get cities.
   *
   * @return array
   *   Select compatible array.
   */
  protected function flatCities() {
    $cities = $this->aemetValues->getCities();
    $resp = [];

    foreach ($cities as $auto) {
      foreach ($auto['provinces'] as $provCode => $province) {
        foreach ($province['cities'] as $cityId => $city) {
          $resp[$auto['label'] . ' > ' . $province['label']][$provCode . $cityId] = $city['label'];
        }
      }
    }

    return $resp;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['city'] = $form_state->getValue('city');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    try {
      // Allow alter configured city.
      $cityToShow = $this->configuration['city'];
      $this->moduleHandler->alter('aemet_block_city', $cityToShow);
      // Module configuration.
      $modSettings = $this->configFactory->get('aemet.settings');

      // Prepare AEMET API configuration.
      $aemetCfg = new Configuration();
      $aemetCfg->setApiKey('api_key', $modSettings->get('api_key'));

      // Call authorization endpoint.
      $pred = new PrediccionesEspecificasApi(NULL, $aemetCfg);
      $mod200 = $pred->prediccinPorMunicipiosDiariaTiempoActual_($cityToShow);
      // Call authorized endpoint to get real data.
      // @see $this->aemetValues->getFinalMetaData($mod200); response to
      // get help.
      $data = $this->aemetValues->getFinalData($mod200);

      // Get city name.
      $city = $this->aemetValues->getCity(substr($cityToShow, 0, 2), substr($cityToShow, 2));
      // Allow hook_block_views_alter know de weather data.
      $build['content']['#aemet_data'] = $data;
      $build['#attached']['library'][] = 'aemet/aemet_styles';
      // $data['elaborado'] : Elaboration date.
      // $data['origen']['copyright'] : Copyright.
      // $data[0]['prediccion']['dia'][0]['estadoCielo'][0]['value'].
      // $data[0]['prediccion']['dia'][0]['temperatura']['minima'].
      // $data[0]['prediccion']['dia'][0]['temperatura']['maxima'].
      // Build block.
      $build['content']['container'] = [
        '#type' => 'container',
        '#prefix' => '<div class="weather-block-container">',
        '#suffix' => '</div>',
      ];

      $modPath = drupal_get_path('module', 'aemet');
      if (!is_file($modPath . '/images/color/' . $data[0]['prediccion']['dia'][0]['estadoCielo'][0]['value'] . '.png')) {
        $data[0]['prediccion']['dia'][0]['estadoCielo'][0]['value'] = 'nodata';
      }
      $url = Url::fromUri(
        'base:' . $modPath . '/images/color/' . $data[0]['prediccion']['dia'][0]['estadoCielo'][0]['value'] . '.png',
        ['absolute' => TRUE]
      );
      $build['content']['container']['sky'] = [
        '#markup' => '<img src="' . $url->toString() . '">',
        '#prefix' => '<span class="weather-block-sky">',
        '#suffix' => '</span>',
        '#weight' => 10,
      ];

      $build['content']['container']['weather'] = [
        '#markup' => $city['label'] . ' ' . $data[0]['prediccion']['dia'][0]['temperatura']['minima'] . ' ºC / ' . $data[0]['prediccion']['dia'][0]['temperatura']['maxima'] . ' ºC',
        '#prefix' => '<span class="weather-block-temp">',
        '#suffix' => '</span>',
        '#weight' => 20,
      ];
      $build['content']['copy'] = [
        '#markup' => '© AEMET',
        '#prefix' => '<div class="weather-block-aemet-copy" title="Información elaborada por la Agencia Estatal de Meteorología">',
        '#suffix' => '</div>',
        '#weight' => 30,
      ];
    }
    catch (\Exception $e) {
      $build = [];
      $this->logger->alert($this->t('Remote call error "@message" in line @line of @file: @trace', [
        '@message' => $e->getMessage(),
        '@file' => $e->getFile(),
        '@line' => $e->getLine(),
        '@trace' => $e->getTraceAsString(),
      ]));
    }

    $this->moduleHandler->alter('aemet_block_build', $build);

    return $build;
  }

}

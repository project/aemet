<?php

namespace Drupal\aemet;

use Drupal\Core\KeyValueStore\KeyValueFactory;
use Drupal\Component\Serialization\Json;
use Swagger\Client\Model\Model200;
use GuzzleHttp\Client;

/**
 * Values service.
 */
class Values {

  public static $ccaa = [
    '01' => [
      "aemet_id" => 'and',
      "ine_id" => '01',
      "label" => 'Andalucía',
    ],
    '02' => [
      "aemet_id" => 'arn',
      "ine_id" => '02',
      "label" => 'Aragón',
    ],
    '03' => [
      "aemet_id" => 'ast',
      "ine_id" => '03',
      "label" => 'Asturias',
    ],
    '04' => [
      "aemet_id" => 'bal',
      "ine_id" => '04',
      "label" => 'Ballears, Illes',
    ],
    '05' => [
      "aemet_id" => 'coo',
      "ine_id" => '05',
      "label" => 'Canarias',
    ],
    '06' => [
      "aemet_id" => 'can',
      "ine_id" => '06',
      "label" => 'Cantabria',
    ],
    '07' => [
      "aemet_id" => 'cle',
      "ine_id" => '07',
      "label" => 'Castilla y León',
    ],
    '08' => [
      "aemet_id" => 'clm',
      "ine_id" => '08',
      "label" => 'Castilla - La Mancha',
    ],
    '09' => [
      "aemet_id" => 'cat',
      "ine_id" => '09',
      "label" => 'Cataluña',
    ],
    '10' => [
      "aemet_id" => 'val',
      "ine_id" => '10',
      "label" => 'Comunitat Valenciana',
    ],
    '11' => [
      "aemet_id" => 'ext',
      "ine_id" => '11',
      "label" => 'Extremadura',
    ],
    '12' => [
      "aemet_id" => 'gal',
      "ine_id" => '12',
      "label" => 'Galicia',
    ],
    '13' => [
      "aemet_id" => 'mad',
      "ine_id" => '13',
      "label" => 'Madrid, Comunidad de',
    ],
    '14' => [
      "aemet_id" => 'mur',
      "ine_id" => '14',
      "label" => 'Murcia, Región de',
    ],
    '15' => [
      "aemet_id" => 'nav',
      "ine_id" => '15',
      "label" => 'Navarra, Comunidad Foral de',
    ],
    '16' => [
      "aemet_id" => 'pva',
      "ine_id" => '16',
      "label" => 'País Vasco',
    ],
    '17' => [
      "aemet_id" => 'rio',
      "ine_id" => '17',
      "label" => 'Rioja, La',
    ],
    '18' => [
      "aemet_id" => '',
      "ine_id" => '18',
      "label" => 'Ceuta',
    ],
    '19' => [
      "aemet_id" => '',
      "ine_id" => '19',
      "label" => 'Melilla',
    ],
  ];

  public static $provinces = [
    // "Código Provincia" => "Provincia".
    "01" => 'Araba/Álaba',
    "02" => 'Albacete',
    "03" => 'Alacant/Alicante',
    "04" => 'Almería',
    "33" => 'Asturias',
    "05" => 'Ávila',
    "06" => 'Badajoz',
    "07" => 'Illes Ballears',
    "08" => 'Barcelona',
    "48" => 'Bizkaia',
    "09" => 'Burgos',
    "10" => 'Cáceres',
    "11" => 'Cádiz',
    "39" => 'Cantabria',
    "12" => 'Castelló/Castellón',
    "51" => 'Ceuta',
    "13" => 'Ciudad Real',
    "14" => 'Córdoba',
    "15" => 'A Coruña',
    "16" => 'Cuenca',
    "17" => 'Girona',
    "18" => 'Granada',
    "19" => 'Guadalajara',
    "20" => 'Gipuzkoa',
    "21" => 'Huelva',
    "22" => 'Huesca',
    "23" => 'Jaén',
    "24" => 'León',
    "25" => 'Lleida',
    "27" => 'Lugo',
    "28" => 'Madrid',
    "29" => 'Málaga',
    "52" => 'Melilla',
    "30" => 'Murcia',
    "31" => 'Navarra',
    "32" => 'Oursense',
    "34" => 'Palencia',
    "35" => 'Las Palmas',
    "36" => 'Pontevedra',
    "26" => 'La Rioja',
    "37" => 'Salamanca',
    "38" => 'Santa Cruz de Tenerife',
    "40" => 'Segovia',
    "41" => 'Sevilla',
    "42" => 'Soria',
    "43" => 'Tarragona',
    "44" => 'Teruel',
    "45" => 'Toledo',
    "46" => 'València/Valencia',
    "47" => 'Valladolid',
    "49" => 'Zamora',
    "50" => 'Zaragoza',
  ];

  /**
   * Key value factory.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueFactory
   */
  protected $keyValueFactory;

  /**
   * Cities codes.
   *
   * @var array
   */
  protected $cities;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Construct value class.
   *
   * @param \Drupal\Core\KeyValueStore\KeyValueFactory $keyValueFactory
   *   The key factory.
   * @param \GuzzleHttp\Client $httpClient
   *   The http client.
   */
  public function __construct(
    KeyValueFactory $keyValueFactory,
    Client $httpClient
  ) {
    $this->keyValueFactory = $keyValueFactory;
    $this->httpClient = $httpClient;

    $this->cities = $this->keyValueFactory->get('aemet.values')->get('cities');
    if (empty($this->cities)) {
      // Parse and load cities.
      $modPath = drupal_get_path('module', 'aemet');
      if (($handle = fopen($modPath . "/api/19codmun.csv", "r")) !== FALSE) {
        $isHead = TRUE;
        while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
          // "CODAUTO";"CPRO";"CMUN";"DC";"NOMBRE ".
          if ($isHead) {
            $isHead = FALSE;
          }
          else {
            // Autonomic comunity ID.
            if (!isset($this->cities[$data[0]])) {
              $this->cities[$data[0]] = [
                'label' => self::$ccaa[$data[0]]['label'],
                'provinces' => [],
              ];
            }
            // Province ID.
            if (!isset($this->cities[$data[0]]['provinces'][$data[1]])) {
              $this->cities[$data[0]]['provinces'][$data[1]] = [
                'label' => self::$provinces[$data[1]],
                'cities' => [],
              ];
            }
            $this->cities[$data[0]]['provinces'][$data[1]]['cities'][$data[2]] = [
              // City ID.
              "cmun" => $data[2],
              "dc" => $data[3],
              // Name.
              "label" => $data[4],
            ];
          }
        }
        fclose($handle);
        $this->keyValueFactory->get('aemet.values')->set('cities', $this->cities);
      }
    }
  }

  /**
   * The response contains useful help about data.
   *
   * @param \Swagger\Client\Model\Model200 $mod200
   *   The autenticated response.
   *
   * @return object|bool
   *   The final data.
   */
  public function getFinalMetaData(Model200 $mod200) {
    if ($mod200->getEstado() == 200) {
      return $this->queryUrl($mod200->getMetadatos());
    }
    else {
      return FALSE;
    }
  }

  /**
   * The response contains the data itself.
   *
   * @param \Swagger\Client\Model\Model200 $mod200
   *   The autenticated response.
   *
   * @return object|bool
   *   The final data.
   */
  public function getFinalData(Model200 $mod200) {
    if ($mod200->getEstado() == 200) {
      return $this->queryUrl($mod200->getDatos());
    }
    else {
      return FALSE;
    }
  }

  /**
   * Simple URL loader.
   *
   * @param string $url
   *   The URL.
   *
   * @return object
   *   The response.
   */
  protected function queryUrl($url) {
    $unserialized = NULL;
    $response = $this->httpClient->request(
      'GET',
      $url,
      [
        'headers' => [
          // Allow the server optimize response.
          'Accept-Encoding' => 'gzip, deflate, compress',
          'Accept' => 'application/vnd.api+json',
        ],
        'http_errors' => FALSE,
      ]
    );
    if ($response->getStatusCode() == 200) {
      $body = $response->getBody()->getContents();
      // The try catch is neccesary because the json will be malformed.
      $unserialized = Json::decode(utf8_encode($body));
    }

    return $unserialized;
  }

  /**
   * Get autonomy comunity data.
   *
   * @param string $id
   *   Aemet ID.
   *
   * @return array|bool
   *   The AC label, INE ID and Aemet ID.
   */
  public function getCcAaByAemetId($id) {
    foreach (self::$ccaa as $ccaa) {
      if ($ccaa['aemet_id'] == $id) {
        return $ccaa;
      }
    }

    return FALSE;
  }

  /**
   * Get autonomy comunity data.
   *
   * @param string $id
   *   INE ID.
   *
   * @return array|bool
   *   The AC label, INE ID and Aemet ID.
   */
  public function getCcAaByIneId($id) {
    if (isset(self::$ccaa[$id])) {
      return self::$ccaa[$id];
    }
    else {
      return FALSE;
    }
  }

  /**
   * The autonomic comunities.
   *
   * @return array
   *   The autonomic comunities.
   */
  public function getCcAas() {
    return self::$ccaa;
  }

  /**
   * Get the province label.
   *
   * @param string $id
   *   Aemet ID.
   *
   * @return string|bool
   *   The province label or false.
   */
  public function getProvinceLabel($id) {
    if (!isset(self::$provinces[$id])) {
      return FALSE;
    }
    else {
      return self::$provinces[$id];
    }
  }

  /**
   * The provinces.
   *
   * @return array
   *   The provinces.
   */
  public function getProvinces() {
    return self::$provinces;
  }

  /**
   * The cities.
   */
  public function getCities() {
    return $this->cities;
  }

  /**
   * Get city data.
   *
   * @param string $provId
   *   Province ID.
   * @param string $munId
   *   City ID.
   *
   * @return array|null
   *   The city data.
   */
  public function getCity($provId, $munId) {
    foreach ($this->cities as $codAuto => $auto) {
      foreach ($auto['provinces'] as $proId => $province) {
        if ($proId == $provId) {
          foreach ($province['cities'] as $cityId => $city) {
            if ($cityId == $munId) {
              $city['cod_auto'] = $codAuto;
              $city['cod_prov'] = $proId;
              return $city;
            }
          }
        }
      }
    }

    return NULL;
  }

}
